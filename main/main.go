package main

import (
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"gitee.com/xmitx/ximi"
	"github.com/tjfoc/gmsm/sm2"
	"github.com/tjfoc/gmsm/x509"
)

func main() {
	keyPair, err := sm2.GenerateKey(rand.Reader)
	if err != nil {
		fmt.Printf("Generate KeyPair Error:%s", err.Error())
	}
	prvKey, err := x509.MarshalSm2PrivateKey(keyPair, []byte(""))
	if err != nil {
		return
	}
	pubKey, err := x509.MarshalSm2PublicKey(&keyPair.PublicKey)
	if err != nil {
		return
	}
	fmt.Printf("Private: %s\n", base64.StdEncoding.EncodeToString(prvKey))
	fmt.Printf("Public: %s\n", base64.StdEncoding.EncodeToString(pubKey))

	text, err := ximi.SM2EncryptHex("你好", base64.StdEncoding.EncodeToString(pubKey))
	fmt.Printf("Encoded: %s\n", text)
	decoded, err := ximi.SM2DecryptHex(text, base64.StdEncoding.EncodeToString(prvKey))
	fmt.Printf("Decoded: %s\n", decoded)
	fmt.Printf("err: %s", err.Error())
}
