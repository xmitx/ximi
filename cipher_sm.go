package ximi

import (
	"bytes"
	"crypto/cipher"
	"crypto/rand"
	_ "embed"
	"encoding/base64"
	"encoding/hex"
	"github.com/tjfoc/gmsm/sm3"
	"github.com/tjfoc/gmsm/sm4"
	"github.com/tjfoc/gmsm/x509"
)

/*SM2: RSA类似*/

const (
	Sm2XimiPrivateKey = "MIH8MFcGCSqGSIb3DQEFDTBKMCkGCSqGSIb3DQEFDDAcBAiTzVPYJY/BjwICCAAwDAYIKoZIhvcNAgcFADAdBglghkgBZQMEASoEEA3T9+Mk2hpH6fbxNhJ5T7sEgaBAgv9Iz5nPZQAOIU73kY0MpJ2KN+slHp2Zdn/PbRWrfL5L2Yqv4MF/sn8BLBZBZt6pfhyZ302kRVqZHBYBTOBcGm/afelhbrsHTvD2SOhJm2lJ1bpVNQUgq7ha3w7J6MUBsnsrf72aq/rs+Pv53KKCU41rUBKh0W6yHzhOIC2sOYk4a5U+YE8sdXHHbvYR+AQ9F/H4I/MTCknIS5adxduw"
	Sm2XimiPublicKey  = "MFkwEwYHKoZIzj0CAQYIKoEcz1UBgi0DQgAEX/ExHas7FvcW0G+E8ChaXoO16CwAMFb6gMn8bu97dHTjHKSTOIGbfDn0aE+eoJkU36BTtwWXgH3hvzVm4VCXog=="
)

func SM2EncryptHex(text string, publicKey string) (string, error) {
	if publicKey == "" {
		publicKey = Sm2XimiPublicKey
	}
	keyBytes, err := base64.StdEncoding.DecodeString(publicKey)
	if err != nil {
		return "", err
	}
	key, err := x509.ParseSm2PublicKey(keyBytes)
	if err != nil {
		return "", err
	}
	asn1, err := key.EncryptAsn1([]byte(text), rand.Reader)
	if err != nil {
		return "", err
	}
	return hex.EncodeToString(asn1), nil
}

func SM2DecryptHex(text string, privateKey string) (string, error) {
	if privateKey == "" {
		privateKey = Sm2XimiPrivateKey
	}
	keyBytes, err := base64.StdEncoding.DecodeString(privateKey)
	if err != nil {
		return "", err
	}
	key, err := x509.ParseSm2PrivateKey(keyBytes)
	if err != nil {
		return "", err
	}
	decodeString, err := hex.DecodeString(text)
	if err != nil {
		return "", err
	}
	asn1, err := key.DecryptAsn1(decodeString)
	if err != nil {
		return "", err
	}
	return string(asn1), nil
}

/*SM3: 类似MD5*/

// SM3Hex 类MD5加密
func SM3Hex(target string) string {
	d := []byte(target)
	m := sm3.New()
	m.Write(d)
	return hex.EncodeToString(m.Sum(nil))
}

// SM3Base64 类MD5加密
func SM3Base64(target string) string {
	d := []byte(target)
	m := sm3.New()
	m.Write(d)
	return base64.StdEncoding.EncodeToString(m.Sum(nil))
}

/*SM4: 类似DES*/

// SM4Encrypt SM4加密:对称加密算法
func SM4Encrypt(data string, SM4Key string, SM4iv string) (result string, err error) {
	//字符串转byte切片
	plainText := []byte(data)
	//建议从配置文件中读取秘钥，进行统一管理
	if SM4Key == "" {
		SM4Key = "Uv6tkf2M3xYSRuFv"
	}
	if SM4iv == "" {
		SM4iv = "04TzMuvkHm_EZnHm"
	}
	iv := []byte(SM4iv)
	key := []byte(SM4Key)
	//实例化sm4加密对象
	block, err := sm4.NewCipher(key)
	if err != nil {
		panic(err)
	}
	//明文数据填充
	paddingData := paddingLastGroup(plainText, block.BlockSize())
	//声明SM4的加密工作模式
	blockMode := cipher.NewCBCEncrypter(block, iv)
	//为填充后的数据进行加密处理
	cipherText := make([]byte, len(paddingData))
	//使用CryptBlocks这个核心方法，将paddingData进行加密处理，将加密处理后的值赋值到cipherText中
	blockMode.CryptBlocks(cipherText, paddingData)
	//加密结果使用hex转成字符串，方便外部调用
	cipherString := hex.EncodeToString(cipherText)
	return cipherString, nil
}

// SM4Decrypt SM4解密 传入string 输出string
func SM4Decrypt(data string, SM4Key string, SM4iv string) (res string, err error) {
	if SM4Key == "" {
		SM4Key = "Uv6tkf2M3xYSRuFv"
	}
	if SM4iv == "" {
		SM4iv = "04TzMuvkHm_EZnHm"
	}
	iv := []byte(SM4iv)
	key := []byte(SM4Key)
	block, err := sm4.NewCipher(key)
	if err != nil {
		panic(err)
	}
	//使用hex解码
	decodeString, err := hex.DecodeString(data)
	if err != nil {
		return "", err
	}
	//CBC模式 优点：具有较好的安全性，能够隐藏明文的模式和重复性。 缺点：加密过程是串行的，不适合并行处理。
	blockMode := cipher.NewCBCDecrypter(block, iv)
	//下文有详解这段代码的含义
	blockMode.CryptBlocks(decodeString, decodeString)
	//去掉明文后面的填充数据
	plainText := unPaddingLastGroup(decodeString)
	//直接返回字符串类型，方便外部调用
	return string(plainText), nil
}

// 明文数据填充
func paddingLastGroup(plainText []byte, blockSize int) []byte {
	//1.计算最后一个分组中明文后需要填充的字节数
	padNum := blockSize - len(plainText)%blockSize
	//2.将字节数转换为byte类型
	char := []byte{byte(padNum)}
	//3.创建切片并初始化
	newPlain := bytes.Repeat(char, padNum)
	//4.将填充数据追加到原始数据后
	newText := append(plainText, newPlain...)
	return newText
}

// 去掉明文后面的填充数据
func unPaddingLastGroup(plainText []byte) []byte {
	//1.拿到切片中的最后一个字节
	length := len(plainText)
	lastChar := plainText[length-1]
	//2.将最后一个数据转换为整数
	number := int(lastChar)
	return plainText[:length-number]
}
