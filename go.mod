module gitee.com/xmitx/ximi

go 1.21

require (
	github.com/GUAIK-ORG/go-snowflake v0.0.0-20200116064823-220c4260e85f
	github.com/denisbrodbeck/machineid v1.0.1
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/tjfoc/gmsm v1.4.1
)

require (
	github.com/golang/glog v1.2.1 // indirect
	github.com/google/go-cmp v0.6.0 // indirect
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/gomega v1.33.1 // indirect
	golang.org/x/crypto v0.23.0 // indirect
	golang.org/x/net v0.24.0 // indirect
	golang.org/x/sys v0.20.0 // indirect
	golang.org/x/text v0.15.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
