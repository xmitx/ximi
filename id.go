package ximi

import (
	"crypto/rand"
	"fmt"
	"github.com/GUAIK-ORG/go-snowflake/snowflake"
	"github.com/go-redis/redis"
	"log"
	"time"
)

type IdWorker struct {
	snowflake *snowflake.Snowflake
}

var instance = &IdWorker{snowflake: initSnowFlake()}

func initSnowFlake() *snowflake.Snowflake {
	s, err := snowflake.NewSnowflake(int64(0), int64(0))
	if err != nil {
		_ = fmt.Errorf("init snowflake error: %s\n", err)
	}
	fmt.Println("Default IdWorker Initialized With SnowFlake ...")
	return s
}

// GetIdWorker 获取IdWorker单例
func GetIdWorker() *IdWorker {
	return instance
}

// RedisId Redis生成ID
func (idWorker *IdWorker) RedisId(redisClient *redis.Client) int64 {
	v := redisClient.Incr(RedisIdCacheKey).Val()
	if v > 99999999 {
		redisClient.Set(RedisIdCacheKey, 1000, 0)
		v = 1
	}
	return time.Now().Unix()*100000000 + v
}

// NextId 雪花ID
func (idWorker *IdWorker) NextId() int64 {
	return idWorker.snowflake.NextVal()
}

// NextIdStr 雪花ID(String)
func (idWorker *IdWorker) NextIdStr() string {
	return fmt.Sprintf("%d", idWorker.snowflake.NextVal())
}

// UUID UUID
func (idWorker *IdWorker) UUID() string {
	b := make([]byte, 16)
	_, err := rand.Read(b)
	if err != nil {
		log.Fatal(err)
	}
	return fmt.Sprintf("%x%x%x%x%x",
		b[0:4], b[4:6], b[6:8], b[8:10], b[10:])
}
