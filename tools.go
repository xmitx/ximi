package ximi

import (
	"encoding/hex"
	"encoding/json"
	"fmt"
	"github.com/denisbrodbeck/machineid"
	"github.com/go-redis/redis"
	"net"
	"os"
	"reflect"
	"strconv"
	"strings"
	"time"
)

var IsCheckingLicense bool = false

// RedisGet Redis获取(JSON序列化)
func RedisGet[T any](redisClient *redis.Client, key string, beanPtr *T) error {
	bytes, err := redisClient.Get(key).Bytes()
	if err != nil {
		return err
	}
	if err := json.Unmarshal(bytes, beanPtr); err != nil {
		return err
	}
	return nil
}

// RedisPut Redis保存(JSON序列化)
func RedisPut(redisClient *redis.Client, key string, expire time.Duration, data interface{}) error {
	bytes, err := json.Marshal(data)
	if err != nil {
		return err
	}
	_, err = redisClient.Set(key, bytes, expire).Result()
	if err != nil {
		return err
	}
	return nil
}

// RedisHashGet Redis获取(JSON序列化)
func RedisHashGet[T any](redisClient *redis.Client, key string, field string, beanPtr *T) error {
	bytes, err := redisClient.HGet(key, field).Bytes()
	if err != nil {
		return err
	}
	if err := json.Unmarshal(bytes, beanPtr); err != nil {
		return err
	}
	return nil
}

// RedisHashPut Redis保存(JSON序列化)
func RedisHashPut(redisClient *redis.Client, key string, field string, data interface{}) error {
	bytes, err := json.Marshal(data)
	if err != nil {
		return err
	}
	_, err = redisClient.HSet(key, field, bytes).Result()
	if err != nil {
		return err
	}
	return nil
}

// CheckLicense 检查授权
func CheckLicense(licFileName string, productId string, productVersion string) (Lic, error) {
	if licFileName == "" {
		licFileName = "ximi.lic"
	}
	lic := Lic{}
	if IsCheckingLicense {
		fmt.Printf("Checking,Call Later...")
		return lic, fmt.Errorf("检查进行中%s ", "")
	}
	IsCheckingLicense = true
	/*
		macs, err := g.GetMacs(true)
		if err != nil {
			logrus.Fatalf("获取MAC地址失败: %s\n", err.Error())
			os.Exit(0)
		}
		logrus.Printf("macs:%s", macs)
	*/

	fileBytes, err := os.ReadFile(licFileName)
	if err != nil {
		//logrus.Fatalf("授权文件不存在,请检查: %s\n", err.Error())
		//os.Exit(0)
		return lic, fmt.Errorf("授权文件不存在,请检查: %s\n", err.Error())
	}
	combinedStr := string(fileBytes)
	strArr := strings.Split(combinedStr, "$")
	licBytes, err := hex.DecodeString(strArr[0])
	if err != nil {
		//logrus.Fatalf("授权文件解码失败: %s\n", err.Error())
		//os.Exit(0)
		return lic, fmt.Errorf("授权文件解码失败: %s\n", err.Error())
	}
	str, err := RsaDecodeWithPublicKey(licBytes, "")
	if err != nil {
		//logrus.Fatalf("解析授权文件失败")
		//os.Exit(0)
		return lic, fmt.Errorf("解析授权文件失败: %s\n", err.Error())
	}
	// 解码全称
	nameBytes, err := hex.DecodeString(strArr[1])
	if err != nil {
		//logrus.Fatalf("授权文件解码失败: %s\n", err.Error())
		//os.Exit(0)
		return lic, fmt.Errorf("授权文件解码失败: %s\n", err.Error())
	}
	customerName, err := RsaDecodeWithPublicKey(nameBytes, "")
	if err != nil {
		//logrus.Fatalf("解析授权文件失败")
		//os.Exit(0)
		return lic, fmt.Errorf("解析授权文件失败: %s\n", err.Error())
	}

	// 解码简称
	simpleBytes, err := hex.DecodeString(strArr[2])
	if err != nil {
		//logrus.Fatalf("授权文件解码失败: %s\n", err.Error())
		//os.Exit(0)
		return lic, fmt.Errorf("授权文件解码失败: %s\n", err.Error())
	}
	simpleName, err := RsaDecodeWithPublicKey(simpleBytes, "")
	if err != nil {
		//logrus.Fatalf("解析授权文件失败")
		//os.Exit(0)
		return lic, fmt.Errorf("解析授权文件失败: %s\n", err.Error())
	}

	//解码机器码
	machineBytes, err := hex.DecodeString(strArr[3])
	if err != nil {
		//logrus.Fatalf("授权文件解码失败: %s\n", err.Error())
		//os.Exit(0)
		return lic, fmt.Errorf("授权文件解码失败: %s\n", err.Error())
	}
	machineId, err := RsaDecodeWithPublicKey(machineBytes, "")
	if err != nil {
		//logrus.Fatalf("解析授权文件失败")
		//os.Exit(0)
		return lic, fmt.Errorf("解析授权文件失败: %s\n", err.Error())
	}

	//不采用json的方式来组装授权，字符长度受限
	/*var lic XimiLic
	err = json.Unmarshal([]byte(str), &lic)
	if err != nil {
		logrus.Fatalf("转换授权文件失败: %s\n", err.Error())
		os.Exit(0)
	}*/
	strs := strings.Split(str, "|")
	start, err := strconv.ParseInt(strs[1], 10, 64)
	if err != nil {
		//logrus.Fatalf("解析授权文件失败(start): %s\n", err.Error())
		return lic, fmt.Errorf("解析授权文件失败(start): %s\n", err.Error())
	}
	end, err := strconv.ParseInt(strs[2], 10, 64)
	if err != nil {
		//logrus.Fatalf("解析授权文件失败(end): %s\n", err.Error())
		return lic, fmt.Errorf("解析授权文件失败(end): %s\n", err.Error())
	}
	lic = Lic{
		Mac:                machineId,
		Start:              start,
		End:                end,
		CustomerId:         strs[3],
		ProductId:          strs[0],
		ProductName:        strs[4],
		CustomerName:       customerName,
		CustomerSimpleName: simpleName,
	}

	if lic.ProductId != productId {
		//logrus.Fatalf("授权与产品不一致: 授权: %s -> 产品: %s", ctx.Lic.ProductId, ProductId)
		//os.Exit(0)
		return lic, fmt.Errorf("授权与产品不一致: 授权: %s -> 产品: %s", lic.ProductId, productId)
	}

	dateStr := time.Now().Format("20060102")
	date, err := strconv.ParseInt(dateStr, 10, 64)

	println("======================================")
	println(fmt.Sprintf("== 产品名: %s", strs[4]))
	println(fmt.Sprintf("== 版本号: %s", productVersion))
	println(fmt.Sprintf("== 持有者: %s ", lic.CustomerName))
	println(fmt.Sprintf("== 有效期: %d - %d", lic.Start, lic.End))
	println("**************************************")
	println(fmt.Sprintf("== ID: %s", lic.ProductId))
	println(fmt.Sprintf("== SN: %s", lic.CustomerSimpleName))
	println("======================================")

	if date < lic.Start || date > lic.End {
		//logrus.Fatalf("授权已失效: %d - %d", ctx.Lic.Start, ctx.Lic.End)
		//os.Exit(0)
		return lic, fmt.Errorf("授权已失效: %d - %d", lic.Start, lic.End)
	}
	id := MachineId()
	//logrus.Infof("%s", id)
	fmt.Println(id)
	fmt.Println(lic.Mac)
	IsCheckingLicense = false
	return lic, nil
}

// MachineId 获取设备ID
func MachineId() string {
	id, err := machineid.ProtectedID("XIMI")
	if err != nil {
		return ""
	}

	return id
}

// GetMacs 获取MAC地址
func GetMacs(isSimple bool) (macAddrs []string, err error) {
	netInterfaces, err := net.Interfaces()
	if err != nil {
		fmt.Printf("fail to get net interfaces: %v", err)
		return macAddrs, err
	}
	for _, netInterface := range netInterfaces {
		macAddr := netInterface.HardwareAddr.String()
		if len(macAddr) == 0 {
			continue
		}
		if isSimple {
			macAddrs = append(macAddrs, strings.ToUpper(strings.ReplaceAll(macAddr, ":", "")))
		} else {
			macAddrs = append(macAddrs, macAddr)
		}
	}
	return macAddrs, err
}

// StructCopy 结构体复制
// source 当前有值的结构体
// target 接受值的结构体
// fields 需要的设置的属性
func StructCopy(source interface{}, target interface{}, fields ...string) (err error) {
	sourceKey := reflect.TypeOf(source)
	sourceVal := reflect.ValueOf(source)

	targetKey := reflect.TypeOf(target)
	targetVal := reflect.ValueOf(target)

	if targetKey.Kind() != reflect.Ptr {
		err = fmt.Errorf("被覆盖的数据必须是一个结构体指针")
		return
	}

	targetVal = reflect.ValueOf(targetVal.Interface())

	// 存放字段
	fieldItems := make([]string, 0)

	if len(fields) > 0 {
		fieldItems = fields
	} else {
		for i := 0; i < sourceVal.NumField(); i++ {
			fieldItems = append(fieldItems, sourceKey.Field(i).Name)
		}
	}

	for i := 0; i < len(fieldItems); i++ {
		field := targetVal.Elem().FieldByName(fieldItems[i])
		value := sourceVal.FieldByName(fieldItems[i])
		if field.IsValid() && field.Kind() == value.Kind() {
			field.Set(value)
		}

	}
	return

}

func TypeChange(source string, splitStr string) []int {
	tempArr := strings.Split(source, splitStr)
	result := make([]int, 0)
	for _, item := range tempArr {
		newItem, _ := strconv.Atoi(item)
		result = append(result, newItem)
	}
	return result
}

// IsPathExists 判断文件是否存在
func IsPathExists(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}
	return false
}

func IsEmpty(arg interface{}) bool {
	switch reflect.TypeOf(arg).Kind().String() {
	case "int":
		if arg == 0 {
			return true
		} else {
			return false
		}
	case "string":
		if arg == "" {
			return true
		} else {
			return false
		}
	case "int64":
		if arg == 0 {
			return true
		} else {
			return false
		}
	case "uint8":
		if arg == false {
			return true
		} else {
			return false
		}
	case "float64":
		if arg == 0.0 {
			return true
		} else {
			return false
		}
	case "byte":
		if arg == 0 {
			return true
		} else {
			return false
		}
	case "ptr":
		//反射判空逻辑
		if reflect.ValueOf(arg).IsNil() { //利用反射直接判空
			return true
		} else {
			return false
		}
	case "struct":
		if arg == nil {
			return true
		} else {
			return false
		}
	case "slice":
		s := reflect.ValueOf(arg)
		if s.Len() == 0 {
			return true
		} else {
			return false
		}
	case "array":
		s := reflect.ValueOf(arg)
		if s.Len() == 0 {
			return true
		} else {
			return false
		}
	default:
		return false
	}
}
