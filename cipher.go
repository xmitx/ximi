package ximi

import (
	"bytes"
	"crypto/cipher"
	"crypto/des"
	"crypto/hmac"
	"crypto/md5"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha1"
	"crypto/x509"
	_ "embed"
	"encoding/base64"
	"encoding/hex"
	"encoding/pem"
	"math/big"
)

// HmacSHA1Base64 HmacSHA1加密
func HmacSHA1Base64(key string, data string) string {
	mac := hmac.New(sha1.New, []byte(key))
	mac.Write([]byte(data))
	return base64.StdEncoding.EncodeToString(mac.Sum(nil))
}

// HmacSHA1Hex HmacSHA1加密
func HmacSHA1Hex(key string, data string) string {
	mac := hmac.New(sha1.New, []byte(key))
	mac.Write([]byte(data))
	return hex.EncodeToString(mac.Sum(nil))
}

// MD5Hex MD5加密
func MD5Hex(target string) string {
	d := []byte(target)
	m := md5.New()
	m.Write(d)
	return hex.EncodeToString(m.Sum(nil))
}

// MD5Base64 MD5加密
func MD5Base64(target string) string {
	d := []byte(target)
	m := md5.New()
	m.Write(d)
	return base64.StdEncoding.EncodeToString(m.Sum(nil))
}

// DesEncode Des加密
func DesEncode(src []byte, key string) string { //传递两个参数,src为需要加密的明文，返回[]byte类型数据
	//1：创建并返回一个DES算法的cipher.block接口
	block, err := des.NewCipher([]byte(key))
	//2：判断是否创建成功
	if err != nil {
		panic(err)
	}
	//3：对最后一个明文分组进行数据填充
	src = PKCS5Padding(src, block.BlockSize())
	//4：创建一个密码分组为链接模式的，底层使用DES加密的BLockMode接口
	//    参数iv的长度，必须等于b的块尺寸
	tmp := []byte("helloAAA") //初始化向量
	blockmode := cipher.NewCBCEncrypter(block, tmp)
	//5:加密连续的数据块
	dst := make([]byte, len(src))
	blockmode.CryptBlocks(dst, src)
	//fmt.Println("加密之后的数据:",dst)
	//6:将加密后的数据返回
	return hex.EncodeToString(dst)
}

// DesDecode Des解密
func DesDecode(src string, key string) []byte {
	//1:创建并返回一个使用DES算法的cipher.block接口
	block, err := des.NewCipher([]byte(key))
	//2:判断是否创建成功
	if err != nil {
		panic(err)
	}
	//创建一个密码分组为链接模式的，底层使用DES解密的BlockMode接口
	tmp := []byte("helloAAA")
	blockMode := cipher.NewCBCDecrypter(block, tmp)
	//解密数据
	srcBytes := []byte(src)
	dst := make([]byte, len(srcBytes))
	blockMode.CryptBlocks(dst, srcBytes)
	//5:去掉最后一组填充数据
	dst = PKCS5UnPadding(dst)
	//返回结果
	return dst
}

// PKCS5Padding pkcs5填充
func PKCS5Padding(ciphertext []byte, blockSize int) []byte {
	//1:计算最后一个分组缺多少字节
	padding := blockSize - (len(ciphertext) % blockSize)
	//2:创建一个大小为padding的切片,每个字节的值为padding
	padText := bytes.Repeat([]byte{byte(padding)}, padding)
	//3:将padText添加到原始数据的后边，将最后一个分组缺少的字节数补齐
	newText := append(ciphertext, padText...)

	return newText
}

// PKCS5UnPadding 删除pkcs5填充的尾部数据
func PKCS5UnPadding(origData []byte) []byte {
	//1:计算数据的总长度
	length := len(origData)
	//2:根据填充的字节值得到填充的次数
	number := int(origData[length-1])
	//3:将尾部填充的number个字节去掉
	return origData[:(length - number)]
}

/*RSA部分*/

//go:embed public.pem
var PublicKey []byte
var PrivateKey []byte

// RsaDecode RSA私钥解密
func RsaDecode(str []byte, privateKey string) (string, error) {
	privateKeyBytes := PrivateKey
	if privateKey != "" {
		privateKeyBytes = []byte(privateKey)
	}
	block, _ := pem.Decode(privateKeyBytes)
	parseResult, err := x509.ParsePKCS8PrivateKey(block.Bytes)
	if err != nil {
		return "", err
	}
	prov := parseResult.(*rsa.PrivateKey)
	res, err := rsa.DecryptPKCS1v15(rand.Reader, prov, str)
	if err != nil {
		return "", err
	}
	return string(res), nil
}

// RsaDecodeWithPublicKey RSA公钥解密
func RsaDecodeWithPublicKey(str []byte, publicKey string) (string, error) {
	publicKeyBytes := PublicKey
	if publicKey != "" {
		publicKeyBytes = []byte(publicKey)
	}
	block, _ := pem.Decode(publicKeyBytes)
	parseResult, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		return "", err
	}
	prov := parseResult.(*rsa.PublicKey)
	return string(rsaPublicKeyDecode(prov, str)), nil
}

func rsaPublicKeyDecode(pubKey *rsa.PublicKey, data []byte) []byte {
	c := new(big.Int)
	m := new(big.Int)
	m.SetBytes(data)
	e := big.NewInt(int64(pubKey.E))
	c.Exp(m, e, pubKey.N)
	out := c.Bytes()
	skip := 0
	for i := 2; i < len(out); i++ {
		if i+1 >= len(out) {
			break
		}
		if out[i] == 0xff && out[i+1] == 0 {
			skip = i + 2
			break
		}
	}
	return out[skip:]
}
