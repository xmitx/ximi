package ximi

const (
	// ContentTypeApplicationJson /*Content-Types*/
	ContentTypeApplicationJson = "application/json;charset=UTF-8"
	ContentTypeFormUrlEncoded  = "application/x-www-form-urlencoded"
	ContentTypeFormData        = "multipart/form-data"

	// StatusSuccess Business Status
	StatusSuccess = 0
	StatusFail    = -1

	// HttpHeaderXimiAccessToken http头
	HttpHeaderXimiAccessToken = "X-Access-Token"

	// RedisIdCacheKey Redis分布式ID
	RedisIdCacheKey = "GO:GLOBAL:FACTOR"
)
